
const express = require("express")
const cors = require('cors')
const app = express()

const data = require("../data/recipes.json")
const recipes = require("../data/recipes.json").recipes

app.use(cors())

app.get('/recipes', function (req, res) {
  res.status(200).send(JSON.stringify(recipes))
})

app.all( "*", (req, res) => (res.status(404).send(JSON.stringify({}))) )

const PORT = 8080

app.listen(PORT, function () {
  console.log(`Recipe service started on port ${PORT}!`)
})
