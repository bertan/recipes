
An exploration of React, ES6 and some CSS3 features.

## How to Build

After clonning the repo to your workspace, execute the following commands:

```
npm i -g yarn
```

Then execute the following commands in separate terminals:

```
cd recipes
yarn
yarn start
```

```
cd recipes/server
node app.js
```

## Design Decisions
First of all, I should admit that this architecture is not something I would say production ready. For a project to be production ready, many other factors must be considered. For example, a recipe website should likely be SEO optimized. However, the specs stated that it should be a SPA and this means if we care about SEO, we should render pages server-side and then hydrate the SPA seamlessly when it reaches the client. This is a well known technique, but it is more complex than what is implemented here and because of the time constraint and a lack clear of SEO requirement, it is not chosen. In a real world project, these and other considerations would be discussed beforehand.
 
I chose to use React as the JavaScript framework. My reasons doing so are:
  - React is also the foundation of React Native project
  - React makes it easier to write reusable components
  - We discussed Mendix's plans to use it at the end of the technical interview :)
  
I chose to write most of the CSS by hand and only imported css for reset purposes. Actually, because of the time constraints, I considered using a UI toolkit for React, but the ones I could examine were too opiniated that they would slow me instead of speeding up. Choosing a UI toolkit is a project by itself, because there are many alternatives with various maturity and constraints. I also believe in Mendix, the components must be written from ground-up so that the company would have better control over the UI toolkit. This would allow:
  - Companies own design language to be used
  - Have better integration among components and tools at a lower level
  - Be free of external design decisions made by open source projects
  
I chose to use ES6 as this is the main standard all the major browsers are implementing. There are also stable tools that compile ES6 code to ES5 code for wider browser support. There are some problems with the compiled code in ES5 for debugging production code but this situation is not a deal breaker and I am confident that these issues will be irrelevant in the very near future.

I also planned to use Flow for type safety; however time constraints prevented me from doing so. Also, the simplicity of the project would probably not help showcasing the benefits of a type safety system like Flow.

I chose to use vanilla CSS with some new features becoming available in browsers; namely CSS Variables feature. One of the benefits of tools like SASS and Stylus is the ability to use variables to share and quickly change style rules across UI components. CSS Variables is the standard to solve this issue with vanilla CSS.

As for the OOP aspect of the code; I used composition for the Collection and RecipeList components. IMHO, this is a better and lighter weight solution for reusability of functionality among UI Components. Sure, I could implement an inheritence based approach as in ExtJS and Java Swing, but in web platform this usually results in unnecessary DOM elements to be rendered, slowing down the whole UI. Inheritance is used in higher level Components, extending Rect.Component, to bind to life-cycle events of React component management system. Even then, display-only Components do not extend React.Component but provide a simple function to return a DOM object when supplied with Props.

  
For the content, I had to copy recipes from Epicurious by hand :) This is because APIs I found were either not free, did not meet the requirements or both.
