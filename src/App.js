import React from "react";
import {BrowserRouter as Router, Route} from "react-router-dom";
import {loadRecipes} from "./util/recipeService";
import Recipe from "./Recipe"
import RecipeList from "./RecipeList"

import "./css/app.css";


class App extends React.Component {

  state = {
    recipes: []
  }

  componentDidMount() {
    loadRecipes().then(recipes => this.setState({recipes}))
  }

  render() {
    return (
      <Router>
        <div>

          <div className="header">
            <h1>Turkish Cuisine Recipes</h1>
          </div>

          <div className="flex-grid">

            <RecipeList className="col" items={this.state.recipes}/>

            {/* Recipe Details */}
            <Route path="/:index" render={({match}) => (
              <Recipe recipe={this.state.recipes[match.params.index]}/>
            )}/>
          </div>

        </div>
      </Router>
    )
  }
}

export default App