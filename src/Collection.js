import React from "react";

const Collection = (ListItem) => class extends React.Component {
  constructor() {
    super()

    this.state = {val: ""}
    this.filter = this.filter.bind(this)
  }

  filter() {
    this.setState({val: this.refs.search.value.toLowerCase()})
  }

  render() {

    return (
      <div className="col collection-wrapper">
        <style>
          {`.collection > :not([data-fkey *= ${this.state.val}]){ display:none; }`}
        </style>

        {/* Search bar */}
        <div className="flex-grid">
          <input ref="search"
            type="text"
            placeholder="Search"
            className="search col2"
            onChange={this.filter}/>
        </div>

        <div className="collection">

          <span>{this.state.val}</span>

          {this.props.items.map((item, index) => <ListItem key={index} index={index} recipe={item}/>)}
        </div>
      </div>)
  }
}

export default Collection