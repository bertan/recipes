const baseUrl = 'http://localhost:8080/recipes'

export const loadRecipes = () => {
  return fetch(baseUrl).then(res => (res.json()))
}
