import React from "react";


const Recipe = ({recipe}) => {
  let image = <img src="http://placehold.it/350x250"/>

  if (recipe) {
    if (recipe.image) {
      image = <img src={recipe.image}/>
    }
    return <div className="col2 details-pane">
      <h1 className="recipe-title">{recipe.title}</h1>
      {image}
      <div className="details-ingredients" dangerouslySetInnerHTML={{__html: recipe.ingredients}}/>
      <div className="details-instructions" dangerouslySetInnerHTML={{__html: recipe.instructions}}/>
    </div>
  } else {
    return <div className="col2 details-pane">
      <p>No recipe is selected</p>
    </div>
  }
}


export default Recipe;