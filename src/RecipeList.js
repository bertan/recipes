import React from "react";
import {NavLink} from "react-router-dom";
import VegIcon from "./img/veg.png";
import Collection from "./Collection"

const RecipeListItem = (props) => {
  let recipe = props.recipe
  let fkey = {'data-fkey': recipe.title.toLowerCase()}
  let image = "http://placehold.it/80x80"

  if (recipe && recipe.image) {
    image = recipe.image
  }

  return (
    <NavLink to={`/${props.index}`}
             className="collection-item"
             {...fkey}
             activeClassName="active">
      <div className="collection-item-image"
           style={{backgroundImage: `url(${image})`}}/>
      <div className="col collection-item-desc">
        <h3 className="collection-item-title">{recipe.title}</h3>
        {recipe.isVegetarian &&
        < img src={VegIcon}
              className="collection-item-veg-icon"
              title="Vegetarian meal"/>
        }
      </div>
    </NavLink>
  )
}

const RecipeList = Collection(RecipeListItem)

export default RecipeList